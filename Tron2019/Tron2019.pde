
TronGame game;
Settings SETTINGS;

void setup() {
  size(960, 540, P3D);
  frameRate(60);
  SETTINGS = new Settings();
  game = new TronGame(3);
  startGame();
  
}

void startGame() {
  game.init(5);
  game.players[0].autoPilot=false;
}

void keyPressed() {
  game.keyPressed(key);
}

void draw() {
  background(0);

  game.tick();//1.0/frameRate);
  game.draw();

  int alivePlayers=0;
  for (int i=0; i<game.players.length; i++)
    alivePlayers += game.players[i].alive?1:0;
  if (alivePlayers==0) {
    startGame();
  }
}
