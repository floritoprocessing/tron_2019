class TronGame {

  float unitSize;
  int fieldWidth;
  int fieldHeight;
  FieldUnit[][] field;
  ArrayList<FieldUnit> fieldToDraw;

  Player[] players;

  TronGame(float unitSize) {
    this.unitSize = unitSize;
    this.fieldWidth = (int)(width/unitSize);
    this.fieldHeight = (int)(height/unitSize);
    initField();
  }


  void init(int nrOfPlayers) {
    initField();
    initPlayers(nrOfPlayers);
    startPlayers();
  }
  
  void initField() {
    field = new FieldUnit[fieldWidth][fieldHeight];
    for (int x=0;x<fieldWidth;x++) {
      for (int y=0;y<fieldHeight;y++) {
        field[x][y] = new FieldUnit(x,y);
      }
    }
    fieldToDraw = new ArrayList<FieldUnit>();
  }

  void initPlayers(int nrOfPlayers) {
    players = new Player[nrOfPlayers];
    for (int i=0; i<players.length; i++) {
      PVector p = getStartPoint((float)i/(float)nrOfPlayers);
      players[i] = new Player(i, (int)p.x, (int)p.y, SETTINGS.playerColors[i]);
      occupyUnityByPlayer((int)p.x, (int)p.y, i);
    }
  }

  void startPlayers() {
    for (int i=0; i<players.length; i++) {
      int dir = (int)random(4);
      players[i].setDir(dir);
    }
  }
  
  
  
  
  /*
  *
  *    INTERACTION
  *
  */
  
  
  void keyPressed(char k) {
    for (Player p:players) {
      p.keyPressed(k);
    }
  }


  /*
  *
  *    UPDATE
  *
  */


  void tick() {
    for (int i=0; i<players.length; i++) {
      
      players[i].tick(field, fieldToDraw);
    }
  }



  /*
  *
   *    DRAWING
   *
   */



  void draw() {
    drawGrid();
    drawFieldUnits();
    drawPlayers();
  }



  void drawGrid() {
    pushMatrix();
    translate(0,0,-0.5);
    noFill();
    strokeWeight(SETTINGS.gridWeight);
    stroke(SETTINGS.gridColor);
    beginShape(LINES);
    for (float x=SETTINGS.gridSize; x<width; x+=SETTINGS.gridSize) {
      vertex(x, 0);
      vertex(x, height);
    }
    for (float y=SETTINGS.gridSize; y<height; y+=SETTINGS.gridSize) {
      vertex(0, y);
      vertex(width, y);
    }
    endShape();
    popMatrix();
  }



  void drawFieldUnits() {
    noStroke();
    beginShape(QUADS);
    for (FieldUnit fu:fieldToDraw) {
      fill(SETTINGS.playerTraceColors[fu.playerIndex]);
      float x0 = fu.fieldX * unitSize;
      float y0 = fu.fieldY * unitSize;
      float x1 = x0 + unitSize;
      float y1 = y0 + unitSize;
      vertex(x0,y0);
      vertex(x0,y1);
      vertex(x1,y1);
      vertex(x1,y0);
    }
    endShape();
  }
  
  
  
  void drawPlayers() {
    noStroke();
    rectMode(CORNER);
    for (int i=0; i<players.length; i++) {
      if (players[i].alive) {
        fill(players[i].col);
        rect(players[i].x*unitSize, players[i].y*unitSize, unitSize, unitSize);
      }
    }
  }



  /*
  *
   *    UTILS
   *
   */

  PVector getStartPoint(float percentageOnCircle) {
    float xCenter = fieldWidth*0.5;
    float yCenter = fieldHeight*0.5;
    float min = min(fieldWidth, fieldHeight);
    float hei = min*0.35;
    float wid = min*0.35;
    PVector out = new PVector(
      xCenter + wid * cos(TWO_PI*percentageOnCircle), 
      yCenter + hei * sin(TWO_PI*percentageOnCircle)
      );
    return out;
  }
  
  void occupyUnityByPlayer(int ix, int iy, int playerIndex) {
    field[ix][iy].playerIndex = playerIndex;
    fieldToDraw.add( field[ix][iy] );
  }
}
