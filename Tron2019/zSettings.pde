class Settings {
  
  float gridSize = 20;
  int gridColor = color(20,20,25);
  float gridWeight = 1;
  
  int[] playerColors = new int[] {
    color(255,0,0), color(0,255,0), color(128,128,255), color(128,128,32), color(0,128,128)
  };
  int[] playerTraceColors = new int[] {
    color(128,0,0), color(0,128,0), color(64,64,128), color(64,64,16), color(0,64,64)
  };
  
  // left right
  char[][] playerControls = new char[][] {
    new char[] { 'a', 'd'},
    new char[] {},
    new char[] {},
    new char[] {},
    new char[] {}
  };
  
  //float playerSpeed = 10;
}
