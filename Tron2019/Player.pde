import java.util.Arrays;

class Player {

  int index;
  int x, y;
  int col;  
  int dir;
  int mx=0, my=0;
  boolean alive = true;
  boolean autoPilot = true;

  Player(int index, int x, int y, int col) {
    this.index = index;
    this.x=x;
    this.y=y;
    this.col = col;
    //println("player "+index+" start at "+x+"/"+y);
  }
  
  
  /*
  *
  *    INTERACTION
  *
  */
  
  
  void keyPressed(char k) {
    if (!autoPilot) {
      char[] controls = SETTINGS.playerControls[index];
      if (controls.length>1) {
        if (k==controls[0]) setDir(dir-1);
        else if (k==controls[1]) setDir(dir+1);
      }
      //int dirIndex=-1;
      //char[] controls = SETTINGS.playerControls[index];
      //for (int i=0;i<controls.length;i++)
      //  if (controls[i]==k) {
      //    dirIndex=i;
      //    break;
      //  }
      //if (dirIndex!=-1) {
      //  setDir(dirIndex);
      //}
    }
  }
  
  
  

  void setDir(int dir) { 
    while (dir<0) dir+=4;
    while (dir>3) dir-=4;
    this.dir = dir;
    // right, down, left, up
    mx = dir==0 ? 1 : (dir==2 ? -1 : 0);
    my = dir==1 ? 1 : (dir==3 ? -1 : 0);
  }

  void changeRandomDir() {
    int offset = (int)random(2);
    offset = offset * 2 - 1; // -1;;1
    int newDir = dir+offset;
    //if (newDir<0) newDir+=4; 
    //else if (newDir>=4) newDir-=4;
    setDir(newDir);
  }

  void tick(FieldUnit[][] field, ArrayList<FieldUnit> fieldToDraw) {
    if (alive) {
      
      if (autoPilot && random(100)<4) {
        changeRandomDir();
      }
      
      int futX = x + mx;//*seconds;
      int futY = y + my;//*seconds;
      //println("player "+index+" moving from "+x+"/"+y+" to "+futX+"/"+futY);

      if (futX<0 || futX>=field.length || futY<0 || futY>=field[0].length) {
        die();
        return;
      }
      
      FieldUnit fu = field[futX][futY];
      if (fu.playerIndex!=-1) {
        die();
        return;
      } else {
        fu.playerIndex = index;
        fieldToDraw.add(fu);
      }

      x = futX;
      y = futY;
      
    }
  }

  void die() {
    alive = false;
    //println("player "+index+" died");
  }
}
